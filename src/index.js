// src/index.js

function extractColorsFromCSS(cssText) {
  const colorProperties = ['color', 'background-color', 'border-color'];
  const colors = {};

  const colorRegex = /(?:#(?:[0-9a-fA-F]{3}){1,2}|rgba?\([\d\s,]+\)|hsla?\([\d\s,%]+\)|\b(?:red|blue|green|yellow|orange|purple|pink)\b)/g;

  colorProperties.forEach((property) => {
    const propertyRegex = new RegExp(`${property}[^;]+`);
    const matches = cssText.match(propertyRegex);

    if (matches) {
      matches.forEach((match) => {
        const colorMatch = match.match(colorRegex);
        if (colorMatch) {
          colors[property] = colorMatch[0];
        }
      });
    }
  });

  return colors;
}

function extractColorsFromCSSFiles() {
  const { styleSheets } = document.styleSheets;
  const colors = {};

  for (let i = 0; i < styleSheets.length; i++) {
    const styleSheet = styleSheets[i];

    // Filtra solo las hojas de estilo vinculadas
    if (styleSheet.href) {
      // Descarga y procesa cada archivo CSS
      fetch(styleSheet.href)
        .then((response) => response.text())
        .then((cssText) => {
          const fileColors = extractColorsFromCSS(cssText);
          Object.assign(colors, fileColors);
        })
        .catch((error) => console.error('Error al descargar CSS:', error));
    }
  }

  return colors;
}

document.addEventListener('DOMContentLoaded', () => {
  const colors = extractColorsFromCSSFiles();
  console.info(colors);
});
