# v.0.0.3 - Implement Basic Color Extraction

- Add initial version to chrome//extensions
- Redefine ./webpack.config.js
- npm install copy-webpack-plugin --save-dev
- Define package and watch scripts in package.json
- touch and define manifest.json
- touch and define ./src/index.html
- src/index.js color extraction

# v.0.0.2 - Integrate static code analyzers

- Add lint-staged and husky statements to package.json
- npm install husky lint-staged --save-dev
- Define lint and format scripts in package.json
- npm install --save-dev prettier
- npx eslint --init
- npm install eslint --save-dev
- touch .editorconfig

# v.0.0.1 - Initial structure with Webpack and Babel

- touch ./src/index.js
- mkdir ./src
- Define start and build scripts in package.json
- touch and define webpack.config.js
- touch and define .babelrc
- touch and define .gitignore
- npm install @babel/core @babel/preset-env babel-loader --save-dev
- npm install webpack webpack-cli webpack-dev-server --save-dev
- npm init -y
